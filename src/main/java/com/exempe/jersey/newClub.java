package com.exempe.jersey;

public class newClub {
    private String nom;
    private String ville;
    private Integer cdposttale;
    public newClub (String nom, String ville, Integer cdposttale){
        this.nom=nom;
        this.ville=ville;
        this.cdposttale=cdposttale;
    }

    public String getNom() {
        return nom;
    }

    public String getVille() {
        return ville;
    }

    public Integer getCdposttale() {
        return cdposttale;
    }
}
