package com.exempe.jersey;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


@Path("/user")
public class UserService extends BDD  {

    @POST
    @Path("/con")
    @Produces(MediaType.APPLICATION_JSON)
    public String postCon (@QueryParam("nom") String nom, @QueryParam("pass") String pass){
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        Map<String, String> response = new HashMap<String, String>();
        String jsonInString = null; // var de retour
         // erreur quelque par ici
        try {
            Connection con=getConnexion();
            PreparedStatement sql = con.prepareStatement("SELECT * FROM users WHERE email=? AND mdp=?");
            sql.setString(1, nom);
            sql.setString(2, pass);
            ResultSet req =sql.executeQuery();
            if (req.next()){
                String ut=req.getString("email");
                Integer id=req.getInt("id_users");
                Integer idtype=req.getInt("id_type_user");
                connectUser u = new connectUser(ut, id, idtype);
                response.put("user", u.getName()); // création d'une clef + appel de getNom dans la class user
                response.put("type", "u.getId_type()");
                response.put("con", "1");
            } else {
                response.put("user", "email ou mot de passe incorrect");
                response.put("con", "0");
            }

        } catch (SQLException e) {
            e.printStackTrace();
    }

        try {
            jsonInString = mapper.writeValueAsString(response);
        } catch (Exception e) {
            return e.getMessage();
        }

        return jsonInString;
    }
}
