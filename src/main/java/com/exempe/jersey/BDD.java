package com.exempe.jersey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static java.lang.Class.*;

public class BDD {
    public Connection getConnexion(){
        Connection con=null;
        try {
            forName("com.mysql.jdbc.Driver"); //appel la method
            String url = ("jdbc:mysql://localhost/asfoot");  //variable qui simplifie le chemin
            String userdb = ("root");
            String pwd = ("");

            con = DriverManager.getConnection(url, userdb, pwd); //création de la connection

        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }
}
