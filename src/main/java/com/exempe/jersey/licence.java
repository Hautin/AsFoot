package com.exempe.jersey;

public class licence {
    private String type_licence;
    private String nom;
    private String email;
    private Integer id_licenciee;
    public  licence(String type_licence, String nom, String email, Integer id_licenciee){
        this.type_licence=type_licence;
        this.nom=nom;
        this.email=email;
        this.id_licenciee=id_licenciee;
    }

    public String getType_licence() {
        return type_licence;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public Integer getId_licenciee() {
        return id_licenciee;
    }
}
