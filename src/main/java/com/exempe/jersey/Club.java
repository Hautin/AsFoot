package com.exempe.jersey;

public class Club {
    private Integer id;
    private String nom;
    private String ville;
    private Integer cdposttale;
    public Club (Integer id, String nom, String ville, Integer cdposttale){
        this.id=id;
        this.nom=nom;
        this.ville=ville;
        this.cdposttale=cdposttale;
    }
    // accesseur
    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getVille() {
        return ville;
    }

    public Integer getCdposttale() {
        return cdposttale;
    }
    // modifacateur
    public void setId(Integer id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setCdposttale(Integer cdposttale) {
        this.cdposttale = cdposttale;
    }
}
