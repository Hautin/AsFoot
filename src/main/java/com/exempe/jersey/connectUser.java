package com.exempe.jersey;

public class connectUser {
    private String name;
    private  Integer id;
    private Integer id_type;

    public connectUser(String nom, Integer id, Integer type){
        this.name=nom;
        this.id=id;
        this.id_type=type;
    }
    public String getName(){
        return name;
    }

    public Integer getId() {

        return id;
    }

    public Integer getId_type() {

        return id_type;
    }
}
