package com.exempe.jersey;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */

@Path("/club")
public class ClubService extends BDD {
    // select all club
    @POST
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public String PostAll () throws SQLException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        Map<String, String> response = new HashMap<String, String>();

        String jsonInString = null; // var de retour
        //response.put("erreur", "insertion");
        ArrayList clubArrayList = new ArrayList();
        try {
            Connection con= getConnexion();
            PreparedStatement sql = con.prepareStatement("SELECT * FROM clubs");
            ResultSet req =sql.executeQuery();
            while (req.next()){
                int id=req.getInt("id_clubs");
                String name=req.getString("nom_clubs");
                String ville=req.getString("ville");
                int cdpostale=req.getInt("code_postale");
                Club n= new Club(id, name, ville, cdpostale);
                //response.put("name", n.getNom());
                clubArrayList.add(n);

            }
        } catch (Exception e) {
            return e.getMessage();
        }
        try {
            jsonInString = mapper.writeValueAsString(clubArrayList);
        } catch (Exception e) {
            return e.getMessage();
        }
        return  jsonInString;
    }
    // all licence d'un club
    @POST
    @Path("/alllicence")
    @Produces(MediaType.APPLICATION_JSON)
    public String PostAlllicence (@QueryParam("id") Integer id) throws SQLException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String jsonInString = null; // var de retour
        ArrayList clubArrayList = new ArrayList();
        try {
            Connection con= getConnexion();
            PreparedStatement sql = con.prepareStatement("SELECT * FROM users WHERE id_club=?");
            sql.setInt(1, id);
            ResultSet req =sql.executeQuery();
            while (req.next()){
                String typel=req.getString("type_user");
                String nom=req.getString("nom");
                String email=req.getString("email");
                Integer id_licenciee=req.getInt("id_users");
                licence licence= new licence(typel,nom,email,id_licenciee);
                //response.put("name", n.getNom());
                clubArrayList.add(licence);

            }
        } catch (Exception e) {
            return e.getMessage();
        }
        try {
            jsonInString = mapper.writeValueAsString(clubArrayList);
        } catch (Exception e) {
            return e.getMessage();
        }
        return  jsonInString;
    }
    // nouveau club
    @POST
    @Path("/newclub")
    @Produces(MediaType.APPLICATION_JSON)
    public String PostNewClub (@QueryParam("nom") String nom, @QueryParam("ville") String ville, @QueryParam("cdpostale") Integer cdpostale) throws SQLException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        Map<String, String> response = new HashMap<String, String>();
        String jsonInString = null; // var de retour
        //response.put("erreur", "insertion");
        try {
            Connection con= getConnexion();
            PreparedStatement nsql=con.prepareStatement("INSERT INTO clubs (nom_clubs, ville, code_postale) VALUES (?, ?, ?)");
            nsql.setString(1, nom);
            nsql.setString(2, ville);
            nsql.setInt(3, cdpostale);
            int res=nsql.executeUpdate();
            System.out.println(res);
            response.put("rep", "tout ces bien passer");
        } catch (Exception e) {
            return e.getMessage();
        }
        try {
            jsonInString = mapper.writeValueAsString(response);
        } catch (Exception e) {
            return e.getMessage();
        }
        return  jsonInString;
    }
}
