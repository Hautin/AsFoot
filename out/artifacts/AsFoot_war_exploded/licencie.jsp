<%--
  Created by IntelliJ IDEA.
  User: Pierreh
  Date: 13/04/2018
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Joueurs</title>
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

</head>
<body>
<header class="header" id="top">
    <div class="allhead">
        <div class="first-head">
            <div class="row">
                <div id="center" class="col-sm-4 col-md-4 col-lg-2 offset-lg-1">
                    <img id="timg-head" src="css/images/cageg.jpg"/>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-6">
                    <a id="center" href="/admin.jsp"><h1>AsFootball</h1></a>
                </div>
                <div id="center" class="col-sm-4 col-md-4 col-lg-2">
                    <img id="timg-head" src="css/images/caged.jpg" />
                </div>
            </div>
        </div>
        <div class="navigation">
            <div class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                    <div class="nav navbar-nav">
                        <button type="button" class="btn btn-primary" id="club">Clubs</button>
                    </div>
                    <div class="nav navbar-nav">
                        <button type="button" class="btn btn-primary" id="compte" >Compte</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="row">
    <div class="col-lg-2 offset-lg-2" id="center">
        <button type="button" class="btn btn-primary" id="amembre" >Ajouter un membre</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 offset-lg-2 id="center">
        <table id="table-clubs" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Type de licence</th>
                <th>Nom</th>
                <th>Adresse Email</th>
                <th>Details</th>
            </tr>
            </thead>
            <tbody id="result-club">

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
<script>
    // lien
    $(document).ready(function () {
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }

        var idclub=$.urlParam('id');
        $.ajax({
            type:"POST",
            url:"AsFoot/club/alllicence?id="+idclub,
            success: function(data){
                for (var i = 0; i < data.length ; i++){
                    $('#result-club').append('<tr id="'+data[i].id_licenciee+'"><td>'+data[i].type_licence+'</td><td>'+data[i].nom+'</td><td>'+data[i].email+'</td><td><a href="detail-licencie.jsp.jsp?id='+data[i].id_licenciee+'"><button type="button" class="btn btn-success">En savoir +</button></a></td></tr>');
                }
            }
        });
        // menu
        $('#admin').click(function(){
            window.location="admin.jsp";
        });
        $('#club').click(function(){
            window.location="clubs.jsp";
        });
        $('#compte').click(function(){
            window.location="compte.jsp";
        });
        $('#amembre').click(function(){
            window.location="newmembre.jsp?id="+idclub;
        });
    });
</script>