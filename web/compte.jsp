<%--
  Created by IntelliJ IDEA.
  User: Pierreh
  Date: 13/04/2018
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Compte</title>
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

</head>
<body>
<header class="header" id="top">
    <div class="allhead">
        <div class="first-head">
            <div class="row">
                <div id="center" class="col-sm-4 col-md-4 col-lg-2 offset-lg-1">
                    <img id="timg-head" src="css/images/cageg.jpg"/>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-6">
                    <a id="center" href="/admin.jsp"><h1>AsFootball</h1></a>
                </div>
                <div id="center" class="col-sm-4 col-md-4 col-lg-2">
                    <img id="timg-head" src="css/images/caged.jpg" />
                </div>
            </div>
        </div>
        <div class="navigation">
            <div class="navbar navbar-inverse navbar-static-top">
                <div class="container">
                    <div class="nav navbar-nav">
                        <li><a href="/clubs.jsp"><button type="button" class="btn btn-primary" id="ttmed">Clubs</button></a></li>
                    </div>
                    <div class="nav navbar-nav">
                        <li><a href="/licencie.jsp"><button type="button" class="btn btn-primary" id="" >Licencies</button></a></li>
                    </div>
                    <div class="nav navbar-nav">
                        <li><a href="/finance.jsp"><button type="button" class="btn btn-primary" id="" >Finance</button></a></li>
                    </div>
                    <div class="nav navbar-nav">
                        <li><a href="/compte.jsp"><button type="button" class="btn btn-primary" id="" >Compte</button></a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
</body>
</html>
