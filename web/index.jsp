<%--
  Created by IntelliJ IDEA.
  User: Pierreh
  Date: 07/04/2018
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Connection</title>
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

</head>
<body>
<header class="header" id="top">
    <div class="allhead">
        <div class="first-head">
            <div class="row">
                <div id="center" class="col-sm-4 col-md-4 col-lg-2 offset-lg-1">
                    <img id="timg-head" src="css/images/cageg.jpg"/>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-6">
                    <h1 id="center">AsFootball</h1>
                </div>
                <div id="center" class="col-sm-4 col-md-4 col-lg-2">
                    <img id="timg-head" src="css/images/caged.jpg" />
                </div>
            </div>
        </div>
    </div>
</header>
<div id="con">
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
            <p class="inp-reg">Adresse E-mail :</p>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-2">
            <input type="text" id="login" name="login" maxlength="250">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-4 col-lg-2 offset-lg-3 offset-md-1 offset-sm-1">
            <p class="inp-reg">Mot de passe :</p>
        </div>
        <div class="col-sm-2 col-md-2 col-lg-2">
            <input type="password" id="pass" name="pass" maxlength="250">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-md-10 col-lg-4 offset-lg-5 offset-md-1 offset-sm-1">
            <input id="log" type="submit" name="log" value="Connexion">
        </div>
    </div>
</div>
</body>
<script>
    $( document ).ready(function() {
        $('#log').click(function(){
            var login=$('#login').val();
            var mdp =$('#pass').val();
            $.ajax({
                type:"POST",
                url:"AsFoot/user/con?nom="+login+"&pass="+mdp,
                success: function(data){

                    if(data.con ==1){
                        sessionStorage.setItem('connecter', JSON.stringify(data))
                        window.location="admin.jsp";
                    } else{
                        alert(data.user);
                    }

                }
            });
        });

    });
</script>
</html>
